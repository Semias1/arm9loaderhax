The Homebrew Launcher has many different entrypoints, or methods of launching. The most common is browserhax, which launches the Homebrew Launcher using nothing more than the included browser. This can then be used to install menuhax, which lets you hold a button while the console is booting up to launch the Homebrew Launcher before the rest of the system starts.

**11.0.0 - 11.1.0: You MUST have [downgraded your firmware](Firmware-Downgrade) already.**

#### What you need

+ The Homebrew [Starter Kit](http://smealum.github.io/ninjhax2/starter.zip)
+ One of the games listed in Step 2

#### Instructions

1. Copy _the contents of_ the `starter` folder in `starter.zip` to the root of your SD card, then put the SD card back into your 3DS
2. Use one of the following alternate entrypoints to get into the homebrew launcher:
    + **EUR / JPN / USA**
        + [smashbroshax](https://gbatemp.net/threads/397194/) *(requires Super Smash Bros __(if it is a cart, it must have "Nintendo Network" instead of "amiibo" on the cover, otherwise it is a reprint with an updated game version)__ and __only works on New 3DS__)*
        + [supermysterychunkhax](https://smd.salthax.org/) *(requires Pokemon Super Mystery Dungeon and another 3DS which has the Homebrew Launcher)*
        + [freakyhax](http://plutooo.github.io/freakyhax/) *(requires Freakyforms Deluxe)*
        + [basehaxx](http://mrnbayoh.github.io/basehaxx/) *(requires Pokemon Omega Ruby/Alpha Sapphire ver 1.0/1.4 with the ability to have a secret base and another 3DS which has the Homebrew Launcher)*
        + [BASICSploit](https://mrnbayoh.github.io/basicsploit/) *(requires SmileBASIC USA v3.2.1)*
        + [smilehax](https://plutooo.github.io/smilehax/) *(requires SmileBASIC USA or JPN v3.3.1)*
        + [stickerhax](https://github.com/yellows8/stickerhax) *(requires Paper Mario: Sticker Star and another 3DS which has the Homebrew Launcher)*    
        + [Ninjhax](http://smealum.github.io/ninjhax2/) *(requires Cubic Ninja)*
        + [oot3dhax](https://github.com/yellows8/oot3dhax) *(requires Ocarina of Time 3D and either a powersaves or another 3DS which has the Homebrew Launcher)*    
    + **KOR**
        + [stickerhax](https://github.com/yellows8/stickerhax) *(requires Paper Mario: Sticker Star and another 3DS which has the Homebrew Launcher)*
        + [basehaxx](http://mrnbayoh.github.io/basehaxx/) *(requires Pokemon Omega Ruby/Alpha Sapphire ver 1.0/1.4 with the ability to have a secret base and another 3DS which has the Homebrew Launcher)*

### All versions above 9.2.0 should continue to [9.2.0 Downgrade](9.2.0-Downgrade).

___

### If you are between 9.0.0 and 9.2.0, do the following:

#### What you need

* The Homebrew Launcher and an entrypoint (such as menuhax)
* The latest release of [Decrypt9WIP](https://github.com/d0k3/Decrypt9WIP/releases/latest)

#### Instructions

1. Create a folder named `files9` on the root of your SD card if it does not already exist
3. Copy the `Decrypt9WIP` folder from the Decrypt9WIP zip to `/3ds/` on your SD card
6. Reinsert your SD card into your 3DS
1. Check the system settings, you should be on 9.2.0
2. Launch the Homebrew Launcher using the entrypoint of your choice (such as menuhax)
3. Open Decrypt9WIP **(This can sometimes take a few tries)**
    + If you cannot launch Decrypt9WIP after many tries, you most likely have a partial downgrade and should redo Section II

You can now continue to [Part 2 - 2.1.0 ctrtransfer](Part-2-(2.1.0-ctrtransfer)).
