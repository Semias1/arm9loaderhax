If you started on 2.1.0 **on Old 3DS**, this guide is for you.

If you downgraded to 2.1.0 **on Old 3DS, 2DS, or New 3DS** but for some reason have **no functional NAND backups**, this guide is for you.

If you have already hacked your 3DS before and have a RedNAND based CFW setup, this guide deals exclusively with SysNAND and you should follow all instructions from within or applying to SysNAND. Note that the terms EmuNAND and RedNAND refer to slightly different implementations of [the same concept](http://3dbrew.org/wiki/NAND_Redirection).

#### What you need

* Everything from the "What you need" for [Part 3 - arm9loaderhax](Part-3-(arm9loaderhax))
* The latest release of [Decrypt9WIP](https://github.com/d0k3/Decrypt9WIP/releases/latest)
* The 9.2.0 ctrtransfer image for your device and region     
*(if your device is not from one of these regions, just pick one that matches your device type)*:
  +    <a href="https://plailect.github.io/Guide/9.2.0-20E_ctrtransfer_n3ds.torrent" target="_blank">New 3DS 9.2.0 - EUR - ctrtransfer</a> ([mirror](https://mega.nz/#!lxcWTTCJ!AP8xIzlwdqsGOHRDHpVGhOR-grpPmjFVTTdocpUtt3w)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDYU5OSUg2NS1zajQ))  
  +    <a href="https://plailect.github.io/Guide/9.2.0-20J_ctrtransfer_n3ds.torrent" target="_blank">New 3DS 9.2.0 - JPN - ctrtransfer</a> ([mirror](https://mega.nz/#!llUzGZCK!D_e8RfhzUnBFX9sBv-bQXXBJ1ARftMyrLxPCZsCNvrY)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDcWF2dFNNeERPTFk))    
  +    <a href="https://plailect.github.io/Guide/9.2.0-20U_ctrtransfer_n3ds.torrent" target="_blank">New 3DS 9.2.0 - USA - ctrtransfer</a> ([mirror](https://mega.nz/#!d40QDS7S!OqBnsseshkzyDFNJRtHFm4Xh1J7Tmtggn73kY2jdncM)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDd2FMN181aS1NaVE))   
~
  +    <a href="https://plailect.github.io/Guide/9.2.0-20E_ctrtransfer_o3ds.torrent" target="_blank">Old 3DS or 2DS 9.2.0 - EUR - ctrtransfer</a> ([mirror](https://mega.nz/#!dt1j1abR!-DXxty0Ca9ERXMM_5WPIu9bOkdy3DP1A96VrIxW0FjQ)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDblQ1ZXNZcXkzLWM))    
  +    <a href="https://plailect.github.io/Guide/9.2.0-20J_ctrtransfer_o3ds.torrent" target="_blank">Old 3DS or 2DS 9.2.0 - JPN - ctrtransfer</a> ([mirror](https://mega.nz/#!4p1xVDyZ!VITvzSRRdUfdsgg_t00LSut9ItAB_mEqxeTqk8NgBPM)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDdEQ5NmNUclFsdTQ))    
  +    <a href="https://plailect.github.io/Guide/9.2.0-20U_ctrtransfer_o3ds.torrent" target="_blank">Old 3DS or 2DS 9.2.0 - USA - ctrtransfer</a> ([mirror](https://mega.nz/#!k8lkjYIR!Bf2CMM4iP1VuNwWaCMnl8WvJFNkX2pQ3H4J_5P_tDDA)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDYW1tbUhtblBBdVU))

#### Instructions

4. Copy the 9.2.0 ctrtransfer image `.bin` and `.bin.sha` from the ctrtransfer zip to the `/files9/` folder on your SD card
5. Copy `Decrypt9WIP.bin` to the `/luma/payloads/` folder on your SD card and rename `Decrypt9WIP.bin` to `x_Decrypt9WIP.bin`
6. Reinsert your SD card into your 3DS
7. Open Decrypt9 from arm9loaderhax by holding X during boot
8. Go to "SysNAND Options", then "CTRNAND Transfer", then "Auto CTRNAND Transfer"
9. Select the 9.2.0 ctrtransfer image when prompted by pressing (A)
10. **Backup SysNAND to `NANDmin.bin` when prompted by pressing (A)**
11. Allow the transfer process to proceed automatically, this may take some time
12. Once the transfer is complete, press Select to eject your SD card
13. Put your SD card in your computer, then copy `NANDmin.bin` and `NANDmin.bin.sha` from the `/files9/` folder on your SD card to a safe location; make backups in multiple locations; this backup will save you from a brick if anything goes wrong in the future **(Your backup should match one of the sizes on [this](NAND-Size) page; if it does not, you should delete it and make a new one!)**
14. Delete the 9.2.0 ctrtransfer image `.bin` and `.bin.sha` from the `/files9/` folder on your SD card
15. Reinsert your SD card into your 3DS
16. Press Start to reboot
17. Update your CFW SysNAND to the latest version using system settings (if it is not already)
    + **Yes this is safe, stop opening Github issues about it.**
    + If you copied a `firmware.bin` to `/luma/` earlier, delete it after updating or you won't be able to boot
18. Do [Part 3 - arm9loaderhax - Section V - Injecting FBI](Part-3-(arm9loaderhax)#section-v---removing-rednand-from-your-sd)
19. Do [Part 3 - arm9loaderhax - Section VI - Finalizing setup](Part-3-(arm9loaderhax)#section-vi---finalizing-setup)
20. Read the remainder of [Part 3 - arm9loaderhax](Part-3-(arm9loaderhax))
