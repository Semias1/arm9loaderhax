The first thing this guide will do is get you running Decrypt9, which is a multipurpose toolkit that will allow us to install the version 2.1.0, which contains a vulnerability that is needed for further exploitation of the system.

**If you have already hacked your 3DS before to get an EmuNAND or RedNAND setup, and would like to move the contents of your previous EmuNAND or RedNAND to your new SysNAND CFW, you should follow all instructions and restore your existing RedNAND when prompted once you reach [Part 3 - arm9loaderhax](Part-3-(arm9loaderhax)).**

The two columns of the chart refer to the last number of your version (which corresponds to the browser version installed to the system). If the version is -0 then you do not have a browser, while any number above -0 indicates a browser is installed.

**The "from" and "to" fields are inclusive. This means that, for example, the "from 9.0.0 to 9.2.0" row includes 9.0.0, 9.1.0, and 9.2.0.**

In the case of "5.0.0-0U" for example, you would follow the "No Browser" column and 5.0.0 to 5.1.0 row because the system is on the a system version in that range and has no browser installed.

**For all versions you can [Cart Update](Cart-Update) to a higher version in the same column to follow its instructions instead.**

Select the specific for the version and region you are using from the chart below:

**BOTH = New 3DS and Old 3DS compatible**    
**NEW = New 3DS compatible ONLY**    
**OLD = Old 3DS compatible ONLY**    

**ALL = All regions compatible**    
**EUR = European regions compatible**    
**JPN = Japanese regions compatible**    
**USA = American regions compatible**    
**CHN = Chinese regions compatible**    
**KOR = Korean regions compatible**    
**TWN = Taiwanese regions compatible**    

| From | To | No Browser | Browser |
|:-:|:-:|:-:|:-:|
| 1.0.0 | 1.1.0 | + **ALL - OLD:** <br> [(4.X.X or 6.X.X) Cart Update](Cart-Update) then [Decrypt9 (MSET)](Decrypt9-(MSET)) | - |
| 2.1.0 | 2.2.0 | + **ALL - OLD:** <br> [(4.X.X or 6.X.X) Cart Update](Cart-Update) then [Decrypt9 (MSET)](Decrypt9-(MSET)) | + **ALL - OLD - 2.1.0-4:** <br> [Part 3 - arm9loaderhax](Part-3-(arm9loaderhax))<br>*or*<br> + **ALL - OLD:** <br> [(4.X.X to 8.X.X) Cart Update](Cart-Update) then [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 3.0.0 | 3.0.0 | + **ALL - OLD:** <br> [( 4.X.X or 6.X.X) Cart Update](Cart-Update) then [Decrypt9 (MSET)](Decrypt9-(MSET)) | + **ALL - OLD:** <br> [(4.X.X to 8.X.X) Cart Update](Cart-Update) then [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 4.0.0 | 4.5.0 | + **ALL - OLD:** <br> [Decrypt9 (MSET)](Decrypt9-(MSET)) | + **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 5.0.0 | 5.1.0 | + **ALL - OLD:** <br> [(6.X.X) Cart Update](Cart-Update) then [Decrypt9 (MSET)](Decrypt9-(MSET)) | + **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 6.0.0 | 6.3.0 | + **ALL - OLD:** <br> [Decrypt9 (MSET)](Decrypt9-(MSET)) | + **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 7.0.0 | 7.2.0 | + **ALL - OLD:** <br> [(9.0.0 to 10.7.0) Cart Update](Cart-Update) | + **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 8.0.0 | 8.1.0 | + **ALL - BOTH** <br> [(9.0.0 to 10.7.0) Cart Update](Cart-Update) <br>*or*<br>+ **JPN - NEW:** <br>  [NTR and Cubic Ninja](NTR-and-Cubic-Ninja) | + **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 9.0.0 | 9.2.0 | + **EUR / JPN / USA - BOTH:** <br> [Homebrew Launcher (No Browser)](Homebrew-Launcher-(No-Browser)) | + **EUR / JPN / USA - BOTH:** <br> [Homebrew Launcher (Browser)](Homebrew-Launcher-(Browser)) <br>*or*<br>+ **ALL - OLD:** <br> [Decrypt9 (Browser)](Decrypt9-(Browser)) |
| 9.3.0 | 9.5.0 | + **EUR / JPN / USA - BOTH:** <br> [Homebrew Launcher (No Browser)](Homebrew-Launcher-(No-Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) | + **EUR / JPN / USA - BOTH:** <br> [Homebrew Launcher (Browser)](Homebrew-Launcher-(Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) |
| 9.6.0 | 10.7.0 | + **EUR / JPN / USA - BOTH** <br> **KOR - OLD:** <br> [Homebrew Launcher (No Browser)](Homebrew-Launcher-(No-Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) | + **EUR / JPN / USA - BOTH** <br> **KOR - OLD:** <br> [Homebrew Launcher (Browser)](Homebrew-Launcher-(Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) |
| 11.0.0 | 11.0.0 | + **EUR / JPN / USA - BOTH** <br> **KOR - OLD:** <br> [Firmware Downgrade](Firmware-Downgrade/) then [Homebrew Launcher (No Browser)](Homebrew-Launcher-(No-Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) | + **EUR / JPN / USA - BOTH** <br> **KOR - OLD:** <br> [Firmware Downgrade](Firmware-Downgrade/) then [Homebrew Launcher (Browser)](Homebrew-Launcher-(Browser)) then [9.2.0 Downgrade](9.2.0-Downgrade) |
