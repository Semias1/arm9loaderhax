#### This guide needs *your* help to seed [these](https://github.com/Plailect/Guide/archive/gh-pages.zip) *([rss](https://plailect.github.io/Guide/rss.xml))* torrents!

#### To use the [torrent](https://en.wikipedia.org/wiki/Torrent_file) files in this guide, you will need a torrent client like [this](https://sourceforge.net/projects/trqtw/) one

#### Read all of the introductory pages before proceeding.

___

**Before beginning the guide, you must know the risks of 3DS hacking: EVERY time you modify your system, there is always the potential for an UNRECOVERABLE brick. They're rare, but still a possibility so make sure you follow ALL directions EXACTLY.**

This guide is designed to help a user take a completely unmodified 3DS from full stock
firmware to arm9loaderhax powered custom firmware (which allows almost full control of the device for things such as installing software to the home menu).

**There has been some confusion among users about what devices they own and their names. See [this](Device-Info) page for images of each kind of 3DS.**

This guide will work on New 3DS, Old 3DS, and 2DS in ALL regions on firmware 11.1.0 or below *(except KOR / CHN / TWN New 3DS)*.

This guide uses arm9loaderhax, the newest and best method of Custom Firmware that gives us nearly full control of the system only milliseconds into boot, which is similar to the effect of BootMii for the Wii. The benefits of this are numerous, and as such it is recommended to use this guide over any other that relies on outdated software (such as menuhax or rxTools).

**If everything goes according to plan, you will lose no data and end up with everything that you started with (games, NNID, saves, etc will be preserved).**

A large part of this guide is lengthy NAND dumps and downgrades, so the entire process can take *several* hours thanks to the 3DS's slow processor.

This guide was written by me with the process refined and software developed by the fine folks over at #Cakey on Freenode. See the credits page for the full credits, this guide would not exist without them.

**Your SD card should be [MBR, not GPT](http://www.howtogeek.com/245610/)**.

If you need to format a brand new SD card, you can use [this](http://www.ridgecrop.demon.co.uk/index.htm?guiformat.htm) tool set to an Allocation Unit Size of 32K.
