If you are unable to boot your 3DS, please look for the section relevant to you, and follow the instructions. Once a solution works for you, you can proceed on with the main guide
(The section is fairly long, try using Ctrl+F to search for your issue.)

**If you still cannot solve your issue and need to reach out for help, please paste the contents of all relevant .log files from the root of your SD card into a [Gist](https://gist.github.com/), then come for help prepared with a detailed description of your problem and what you've tried.**

## <a name="twl_broken" />DSi / DS functionality is broken after completing the guide

#### What you need

* The TWL_FIRM `.cia` for your device
    + <a href="https://plailect.github.io/Guide/New_3DS%20TWL_FIRM%20-%20v9936.torrent" target="_blank">`New_3DS TWL_FIRM - v9936.cia`</a> ([mirror](https://mega.nz/#!JhNhCZhI!8k1kN-c6FvQecyBg7sBEdVIFC7foZx3tFrpHMF64Jr0)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDTHZpeWlFWjQ5TDA))
    + <a href="https://plailect.github.io/Guide/Old_3DS%20TWL_FIRM%20-%20v8817.torrent" target="_blank">`Old_3DS TWL_FIRM - v8817.cia`</a> ([mirror](https://mega.nz/#!R9cjyBII!IQ0tBU-uH_NaRrZYH5jXOvZcjFKSIkrDs3yyDjm82k0)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDRXgwUTlOcnFPZkE))
* <a href="https://plailect.github.io/Guide/TWL%20Version%20Data%20-%20v0.torrent" target="_blank">`TWL Version Data - v0.cia`</a> ([mirror](https://mega.nz/#!twtwGLaZ!jr6jZySnZUJnQzGU2HW2_egysMzF7QMhG2avtu7jjLg)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDbEx0RW1MSG5ya3c))
* <a href="https://plailect.github.io/Guide/DS%20Internet%20-%20v2048.torrent" target="_blank">`DS Internet - v2048.cia`</a> ([mirror](https://mega.nz/#!A59yiRzR!VNOn8q3rOZOdz_yUPt60kc2xzorNk33X2LjLPr38oy4)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDWGdQU2lMRV9Wek0))
* <a href="https://plailect.github.io/Guide/DS%20Download%20Play%20-%20v1024.torrent" target="_blank">`DS Download Play - v1024.cia`</a> ([mirror](https://mega.nz/#!F5lE1AhI!gOLs2jXfLavNnTmLT3MfsxayKJYPV5RHgKPpJ5Nvp6M)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDNXhDbDI5XzhFdkk))
* <a href="https://plailect.github.io/Guide/Nintendo%20DS%20Cart%20Whitelist%20-%20v11264.torrent" target="_blank">`Nintendo DS Cart Whitelist - v11264.cia`</a> ([mirror](https://mega.nz/#!IxsnkTgT!bJ25Ne23-U5YX02gjQflKXTcKmffLLxuz0r4yQO7lrI)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDVTc3cW9QRmIxRDQ))

#### Instructions

##### Section I - Prep work

1. Copy `TWL Version Data - v0.cia` to the `/cias/` folder on your SD card
2. Copy `DS Download Play - v1024.cia` to the `/cias/` folder on your SD card
3. Copy `DS Internet - v2048.cia` to the `/cias/` folder on your SD card
4. Copy `Nintendo DS Cart Whitelist - v11264.cia` to the `/cias/` folder on your SD card
5. Copy either `New_3DS TWL_FIRM - v9936.cia`  or `Old_3DS TWL_FIRM - v8817.cia` to the `/cias/` folder on your SD card

##### Section II - Installing titles

1. Open FBI
3. Select "SD"
4. Select "cias"
8. Navigate to `TWL Version Data - v0.cia` and press (A) to install
8. Navigate to `DS Download Play - v1024.cia` and press (A) to install
8. Navigate to `DS Internet - v2048.cia` and press (A) to install
8. Navigate to `Nintendo DS Cart Whitelist - v11264.cia` and press (A) to install
9. Navigate to either `New_3DS TWL_FIRM - v9936.cia`  or `Old_3DS TWL_FIRM - v8817.cia` and press (A) to install
8. Exit with the home button

## <a name="ts_browser" />A browser based exploit is not working
Browser based exploits (such as browserhax or 2xrsa) are often unstable and crash frequently, but they can sometimes be fixed by doing the following steps

1. Open the browser, then open the browser settings
    1. Scroll to the bottom and Initialize Savedata (it also may be called Clear All Save Data)
    2. Try the exploit again

## <a name="ts_safe_a9lh" />System boots directly SafeA9LHInstaller
You copied the wrong `arm9loaderhax.bin` file to your SD card (you were only supposed to copy the `3ds` folder and `SafeA9LHInstaller.dat` file from the SafeA9LHInstaller zip)    

1. Use the correct `arm9loaderhax.bin`
    1. Copy `arm9loaderhax.bin` from the Luma3DS zip to the root of your SD card
    2. Reboot holding select and continue

## <a name="ts_safe_a9lh_screen" />SafeA9LHInstaller shows a glitched screen
This happens occasionally, but the reason is unknown. The buttons will still work, but the screen will be glitched looking

1. Follow instructions as normal
    1. Press (Select) and arm9loaderhax will be installed
    2. The console will reboot
        + If the console does not reboot, wait 10 seconds, then power off your 3DS by holding down the power button

## <a name="ts_dsiware" />After doing the DSiWare Downgrade, my hacked save is gone

1. If you don't have the game at all, link the NNID you bought the game with to **3DS #2** and download it on **3DS #2**
2. On **3DS #1**, do [DSiWare Downgrade - Section III - Installing the save](DSiWare-Downgrade#section-iii---installing-the-save)
3. On **3DS #1**, go to System Settings, "Data Management", "DSiWare", then copy your DSiWare game to your SD card
4. Either put **3DS #1**'s SD card in **3DS #2**, or rename the `Nintendo 3DS` on **3DS #2**'s SD card and copy the `Nintendo 3DS` folder from **3DS #1**'s SD card to **3DS #2**'s SD card
5. On **3DS #2**, go to System Settings, "Data Management", "DSiWare", then copy your DSiWare game to the system
6. Return your SD cards to normal, then continue with the DSiWare Downgrade

## <a name="ts_d9_backup" />Decrypt9 or Hourglass9 won't restore / can't find my NAND backup

1. Make sure you do not have any folder named "Decrypt9" on **the root** of your SD card
3. Try checking your SD card file system with something like `fsck.vfat <sd partition path>` (on *nix) or `CHKDSK <sd drive letter> /F` (on Windows)
4. Try backing up your SD card files, then formatting it and copying them back
5. Try a different SD card

## <a name="ts_sys_down" />Black screen on SysNAND boot

1. Try booting with your SD card out, and then reinserting it after booting.
    1. Power off your 3DS by holding down the power button.
    2. Take out the SD card.
    3. Power on the 3DS.
    4. When the home menu appears, reinsert your SD card.
2. If you have a hardmod and a NAND backup, flash the backup back to SysNAND.
3. Try booting into recovery mode and updating your system.    
    *This probably will not work for an Old 3DS downgraded to 2.1.0*    
    **This will BRICK a New 3DS downgraded to 2.1.0**
    1. Power off your 3DS by holding down the power button.
    2. Hold L+R+A+Up.
    3. Power on the 3DS.
    4. If you enter safe mode, update your 3DS *only if you have an entrypoint for the latest FW version and it is possible to downgrade from it* and attempt the downgrade again.
4. Your 3DS may be bricked. For support, ask for help at [#3dshacks on Rizon IRC](https://gate.omicron.pw/).

## <a name="ts_sys_a9lh" />Black screen on SysNAND boot after installing arm9loaderhax

1. Ensure you have a working payload.
    1. Check for the existence of `arm9loaderhax.bin` in the root of your SD card.
2. Try resetting Luma's config and fix options
    1. Delete `/luma/config.bin` from your SD card
    2. Set your options when it boots
3. Test booting Hourglass9
    1. On Luma3DS, hold Start on boot
4. Try deleting home menu's extdata
    1. Navigate to `/Nintendo 3DS/(32 Character ID)/(32 Character ID)/extdata/00000000/` on your SD card
        + EUR Region: Delete `00000098`
        + JPN Region: Delete `00000082`
        + USA Region: Delete `0000008f`
        + CHN Region: Delete `000000A1`
        + KOR Region: Delete `000000A9`
        + TWN Region: Delete `000000B1`
5. If you did **not** start Part 1 with a NAND between 4.0.0 and 4.5.0, make sure you do not have a `firmware.bin` file in `/luma/`
6. If you started Part 1 with a firmware between 4.0.0 and 4.5.0, make sure you have copied the following `firmware.bin` file to `/luma/`
    + Old 3DS: [`firmware.bin`](https://plailect.github.io/Guide/o3ds_firmware.torrent) ([mirror](https://mega.nz/#!x10nBDYD!1vtMEQw_1R5-1ezzETAqJGoSVXtROldh-809RE9-zR4)) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDeU1NbWtlUURLWGs))
7. Try [this test payload](https://mega.nz/#!YxMiGDhB!VZLv2XPSqFFzEhf4kGMXAdQtSpIGvnp2vu2W1j4o7cc/) ([mirror](https://drive.google.com/uc?export=download&id=0BzPfvjeuhqoDanVaR3FTUTFqNFU)).
    1. Rename `/arm9loaderhax.bin`, if it exists, to something else.
    2. Place the `arm9loaderhax.bin` from the archive linked above in your SD root.
    3. Insert your SD card into your 3DS and power on.
    4. Press (A). Your 3DS should power off; this means arm9loaderhax is working and something else is broken; your device is **not** bricked.
8. If you previously downgraded with Gateway, read [this](https://github.com/AuroraWright/Luma3DS/wiki/FAQ-and-Troubleshooting#i-get-a-black-screen-on-boot-with-an-old-3ds-which-has-been-downgraded-from-the-gateway-menu-in-the-past)
9. Try following [9.2.0 ctrtransfer](9.2.0-ctrtransfer) starting on Step 4.
10. Ask for help at [#3dshacks on Rizon IRC](https://gate.omicron.pw/).

## <a name="ts_sys_blue" />Blue screen on boot (bootrom error)

1. Your 3DS is bricked
2. You will need to get a [hardmod](https://gbatemp.net/threads/414498/) or repair / replace the device
